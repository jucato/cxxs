# CXXS - A C++ Scripting Project

CXXS (Cxx Scripting or "Success") is a simple and crude wrapper around a C++ compiler (only GCC and Clang are supported for now) and a set of header files designed to facilitate the rapid prototyping and testing of C++ programs. In a nutshell, the header files include common headers from the C++ Standard Library, Qt, OpenGL, etc. and defines convenience classes and functions for the most common tasks and objects needed by programmers. The compiler wrapper simply compiles the "script" and immediately runs it (provided there are no errors, of course).

In other words, CXXS provides a set of "batteries" and automates the edit-compile-link-run cycle, giving C++ the semblance of a scripting language.

## Rationale

C++ is best known for optimizing program runtime efficiency and type safety at the expense of programmer time. Scripting languages, on the other hand, empower rapid development but often sacrifice things like type safety, runtime speed, memory efficiency, and all the other strengths that the C++ language can offer. While there are numerous and more professional attempts at addressing this gap in C++, those are often more sophisticated and requires designing around a custom framework or program structure.

In contrast, CXXS is really crude and uses very trivial macros for simple text replacements, mostly to insert the main() function into a "script" to make the program look more like something from Lua or Python. The system itself is pretty much standard "modern" C++ (C++17 to be exact) and should compile like a normal C++ program as long as the CXXS headers are included. The project actually stands on two hard rules:

1. A CXXS C++ script should be compilable even without the compiler wrapper, using normal g++/clang++ commands, as long as the CXXS headers are included and proper libraries are linked. No extensions or modifications to the script should be needed.
2. The CXXS system shouldn't modify the script itself outside of what the preprocessor does. Having to modify the source code in order to compile it would violate rule #1.

**NOTE**: There is one caveat to Rule #1 when a script/program has to be processed first before it gets compiled, as in the case of Qt which may sometimes require running files through MOC (Meta-Object Compiler) before they are compiled. Future versions of CXXS may automate this process or utilize Makefiles or CMake but the rule still holds in principle.

## Uses

- Quickly testing out ideas beyond more than a few lines of code without having to type all the boilerplate necessary for regular C++ programs. For one or two lines of code, CERN ROOT's excellent Cling C++ Interpreter might be a better tool.
- Write quick and dirty utility programs that can be run directly like BASH, Python, or Lua scripts.

CXXS is **NOT** a tool for writing production-ready and distributable code given the performance hits it naturally has (multiple includes, potential recompiling). It can, however, be used for prototyping such programs quickly.

## Why Not <Insert Scripting/Dynamic Language>

There are over a dozen scripting languages and dynamic programming languages, some of which can interface with C++ through bindings. Many of these are mature and well-tested by thriving communities but none are probably more mature and more tested than C++ itself. CXXS tries to bring some of the convenience of scripting and dynamic languages, specifically the idea of "batteries" and rapid iteration cycles, to C++ without losing anything that makes C++ C++.

While many of the larger scripting/dynamic languages are able to interface with C++, they all require some form of binding or translation, some of which are not on the same level of quality as the languages themselves. Since CXXS is really C++, C and C++ libraries can be used directly as one normally does with a regular C++ program, with the option of creating thin class or function wrappers for more common tasks.

Unlike proper scripting languages, however, CXXS doesn't provide a way to "embed" these "scripts" (which are really just plain C++ code) in larger C++ codebases to be compiled and run dynamically. That may come in the future but will require a redesign of the current system.

## Requirements

- Linux, Android using Termux (not tested on Windows, don't have Mac)
- C++17 compliant compiler (tested GCC, Clang)
- "Modules" like Qt, SDL2, OpenGL, etc. have their own dependencies

**NOTE**: CXXS may migrate in the future to use the Qt Core library due to more convenient classes compared to the C++ standard library.

## How to use it

1. Copy the cxxsc "compiler" to a directory in $PATH and make it executable.
2. Copy the cxxs directory that contains the includes in some standard path for C and C++ headers. Modify cxxsc to point to this directory in the Base() CompilerOption function definition.
3. Create a C++ "script" (see the examples directory), make it executable, and run it like a regular command line/shell script.

**IMPORTANT**: The cxStart() macro marks the start of the main() function and built-in argument processing while cxEnd() closes it by returning 0 and adds the closing brace. Both are required. Any declartion/definition that has to happen outside of main() should be written outside of these two markers.

The first run of cxxsc or after any changes to the "compiler" will take a bit longer since the compiler is being compiled into a cached binary (cxxsc.cxc). Likewise, every first run or modification of a C++ "script" involves recompiling the script into a cached version.

## How it Works

There are two main components of CXXS and one particularity of running scripts on Linux that makes the straightforward running of code possible.

### cxxsc - The CXXS "Compiler"

The main program and core of CXXS. It defines a CXXS::Compiler class that practically just calls the actual compiler given a single input file (the "script") and a set of CXXS::CompilerOptions for each of the defined "modules". The CompilerOptions make it easier to add such modules like Qt, SDL, OpenGL, etc. that require specific linked libraries, include paths, or other compiler options.

cxxsc performs a few "tricks" specific to giving the illusion of a C++ scripting system:

- scans the input file/script for included CXXS headers to automatically include those modules during compilation.
- stores the compiled binary in a cache directory (~/.cache/cxxs/ by default) and reuses that for each subsequent run unless the input file has been modified.
- **Temporary**: can also run scripts started with a traditional shebang (#!) but creates a temporary file for it instead of compiling it directly.

cxxsc itself uses the shebang "magic" described below to be a self-executing C++ program. The idea is to have a single "script" as the compiler that can be modified (like any other script) to fit the needs and system setup of the user (compilers, directories, etc.).

### "Modules"

"Modules" are really just headers and are the "batteries" that provide the convenience usually found in other scripting or dynamic languages. They include other commonly used headers for a particular framework or library as well as functions, class definitions, and even objects that make it easier and faster to use them for the most common caes. Because of this, CXXS naturally has slower compile times (and, therefore, total run times) compared to a C++ program that includes only the necessary headers. As it is a simple prototyping and crude scripting tool, users are expected to fine-tune the final version of the code.

Modules follow a simple pair of rules:

- They are located inside the cxxs/ directory and don't end with an ".h" or ".hpp" or similar suffix.
- They must include the common cxxs header (#include <cxxs/cxxs>)

### Pseudo Shebang Magic

TODO: Update this section, it no longer uses the "magic" and uses more standard and portable preprocessor derectives

~~While CXXS pretty much uses standard C++ that can be compiled directly (with the correct includes and link switches), the "magic" comes from the fact that they it can be run directly like a regular script or executable (./scriptname) without explicitly going through the usual compile-run process (or even seeing compile output). This is possible thanks to the way executable files are handled on Linux/UNIX (hence no testing on Windows/Mac/iOS).~~

Credits to https://stackoverflow.com/questions/2482348/run-c-or-c-file-as-a-script for the idea.

~~Instead of the usual shebang that starts with #!, CXXS uses the format `//usr/bin/env cxxsc $0 "$@"; exit`. In a nutshell, this long line gets executed directly, as if it were a shell script that just runs one line, passes the current file ($0) to cxxsc which compiles and runs the code, and then exits so it won't reach the next line at all. Because that line starts with what is technically a C++ comment, this line is ignored when the file is compiled.~~

The cxxsc "compiler" uses a similar trick but with preprocessor directives (#if 0 ... #endif) so that it wouldn't depend on the cxxsc.cxc binary that might not even exist yet.

~~**NOTE:** The shebang has to include the direct path to the env program, which may be different depending on your system (like Termux on Android). It would be nice to be able to just use an ordinary #! but that would break the script's regular compile-ability.~~
