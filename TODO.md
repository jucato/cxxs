# TODO

## Immediate

- [ ] Modularize and clean up cxxsc parts (separate Compiler from source code Assembler)
- [ ] Support passing multiple input source files
- [ ] Add more modules (Lua-like objects, SDL2, OpenGL)

## Mid-range

- [ ] Make modules more independent (don't have to define/add in cxxsc)
- [ ] Find a way to run preprocessing commands needed for some modules (moc for Qt) 
- [ ] Potentially switch to using Qt Core for convenience

## Long-term

- [ ] Make cxxsc handle input from stdin (without affecting script's own input)
- [ ] Export function to generate a trimmed down project (only includes necessary headers and definitions, CMake or Makefile)
- [ ] Runtime compiled C++ (if possible)
